import React, {Component} from "react";
import './Select.css';
export default class Select extends Component {
  render() {
    // looping templates and generating options, assigning the index to the value and handling change from the parent component
    const selectItems = this.props.items.map((item, i) => <option key={item.name} value={i}>{item.name}</option>);
    return (<section className="template-select-container">
      <label htmlFor="template-select">Select a template:</label>
      <select aria-label="Select a template" id="template-select" onChange={this.props.handleChange}>{selectItems}</select>
    </section>);
  }
}
