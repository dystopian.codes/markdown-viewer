import React, {Component} from "react";
import './InputText.css';
export default class InputText extends Component {
  render() {
    //Displaying the current template and handling change from the parent component
    return (<section className="input-text-container">
      <label htmlFor="input-text">Edit the selected template:</label>
      <textarea aria-label="Edit the selected template" id="input-text" value={this.props.content} onChange={this.props.handleChange}/>
    </section>);
  }
}
