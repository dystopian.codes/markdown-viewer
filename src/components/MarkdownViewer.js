import React, {Component} from "react";
import Select from "./Select.js";
import InputText from "./InputText.js";
import OutputText from "./OutputText.js";

export default class MarkdownViewer extends Component {
  constructor(props) {
    super(props);
    //setting up the state with an harcoded source (should be coming from an Api endpoint or a json, depending on the architecture)
    this.state = {
      templates: [
        {
          name: "Template 1",
          content: "# This is a header\n\nAnd this is a paragraph"
        }, {
          name: "Template 2",
          content: "# This is another header\n\nAnd this is another paragraph"
        }
      ],
      content: ""
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  componentDidMount() {
    //initialising the state with the first template
    this.setState({content: this.state.templates[0].content});
  }
  handleSelectChange(event) {
    //assigning the current selected template
    this.setState({
      content: this.state.templates[event.target.value].content
    });
  }
  handleTextChange(event) {
    //visualizing changes from the textarea
    this.setState({content: event.target.value});
  }
  render() {
    return <section>
      <Select items={this.state.templates} handleChange={this.handleSelectChange}/>
      <InputText content={this.state.content} handleChange={this.handleTextChange}/>
      <OutputText content={this.state.content}/>
    </section>;
  }
}
