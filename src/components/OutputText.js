import React, {Component} from "react";
import ReactMarkdown from "react-markdown";
import htmlParser from "react-markdown/plugins/html-parser";
import './OutputText.css';

function parseHtml() {}

export default class OutputText extends Component {
  // Getting the content form the selected template and displaying the converted output, using ReactMarkdown (https://github.com/rexxars/react-markdown)
  render() {
    return (<section className="output-text-container">
      <ReactMarkdown source={this.props.content}/>
    </section>);
  }
}
