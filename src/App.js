import React from 'react';
import './App.css';
import MarkdownViewer from './components/MarkdownViewer.js';

function App() {
  return (<div className="App"><MarkdownViewer/></div>);
}

export default App;
